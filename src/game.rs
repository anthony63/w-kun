use winit::{
    dpi::PhysicalSize, event::DeviceEvent, event::*, event_loop::EventLoop, window::WindowBuilder,
};

pub async fn run() {
    println!("Created by Anthony Sterling-Palmari 9/5/2022");
    let ev_loop = EventLoop::new();
    let win = WindowBuilder::new()
        .with_title("Learning wgpu")
        .with_inner_size(PhysicalSize::new(1280, 720))
        .build(&ev_loop)
        .unwrap();

    let mut state = crate::state::State::new(&win).await;
    let mut last_render_time = instant::Instant::now();
    ev_loop.run(move |ev, _, ctrl_flow| {
        ctrl_flow.set_wait();
        match ev {
            Event::DeviceEvent {
                event: DeviceEvent::MouseMotion { delta },
                ..
            } => {
                if state.mouse_pressed {
                    state.camera_controller.process_mouse(delta.0, delta.1)
                }
            }
            Event::WindowEvent {
                event: ref ev,
                window_id,
            } if window_id == win.id() && state.input(ev) => match ev {
                WindowEvent::CloseRequested => ctrl_flow.set_exit(),
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            virtual_keycode: Some(virtual_code),
                            state: ElementState::Pressed,
                            ..
                        },
                    ..
                } => match virtual_code {
                    VirtualKeyCode::Escape => ctrl_flow.set_exit(),
                    _ => (),
                },
                WindowEvent::Resized(phys_size) => state.resize(*phys_size),
                WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                    state.resize(**new_inner_size)
                }
                _ => (),
            },
            Event::RedrawRequested(win_id) if win_id == win.id() => {
                let now = instant::Instant::now();
                let dt = now - last_render_time;
                last_render_time = now;
                state.update(dt);
                match state.render() {
                    Ok(_) => {}
                    Err(wgpu::SurfaceError::Lost) => state.resize(state.size),
                    Err(wgpu::SurfaceError::OutOfMemory) => ctrl_flow.set_exit(),
                    Err(e) => eprintln!("ERROR: {:?}", e),
                }
            }
            Event::MainEventsCleared => {
                win.request_redraw();
            }
            _ => (),
        }
    });
}
