use game::run;

pub mod buffer;
pub mod camera;
pub mod camera_controller;
pub mod camera_uniform;
pub mod game;
pub mod instance;
pub mod instance_raw;
pub mod math_util;
pub mod projection;
pub mod state;
pub mod texture;

fn main() {
    tracing_subscriber::fmt::init();
    pollster::block_on(run());
}
