use crate::camera::Camera;
use crate::math_util::SAFE_FRAC_PI_2;
use cgmath::{InnerSpace, Rad, Vector3};
use winit::event::{ElementState, KeyboardInput, VirtualKeyCode, WindowEvent};
#[derive(Debug)]
pub struct CameraController {
    vleft: f32,
    vright: f32,
    vforward: f32,
    vbackward: f32,
    vup: f32,
    vdown: f32,
    rot_horiz: f32,
    rot_vert: f32,
    scroll: f32,
    speed: f32,
    sensitivity: f32,
}

impl CameraController {
    pub fn new(speed: f32, sensitivity: f32) -> Self {
        Self {
            vleft: 0.0,
            vright: 0.0,
            vforward: 0.0,
            vbackward: 0.0,
            vup: 0.0,
            vdown: 0.0,
            rot_horiz: 0.0,
            rot_vert: 0.0,
            scroll: 0.0,
            speed,
            sensitivity,
        }
    }
    pub fn process_keyboard(
        &mut self,
        key: winit::event::VirtualKeyCode,
        state: winit::event::ElementState,
    ) -> bool {
        let amount = if state == ElementState::Pressed {
            1.0
        } else {
            0.0
        };
        match key {
            VirtualKeyCode::W | VirtualKeyCode::Up => {
                self.vforward = amount;
                true
            }
            VirtualKeyCode::S | VirtualKeyCode::Down => {
                self.vbackward = amount;
                true
            }
            VirtualKeyCode::A | VirtualKeyCode::Left => {
                self.vleft = amount;
                true
            }
            VirtualKeyCode::D | VirtualKeyCode::Right => {
                self.vright = amount;
                true
            }
            VirtualKeyCode::Space => {
                self.vup = amount;
                true
            }
            VirtualKeyCode::LShift => {
                self.vdown = amount;
                true
            }
            _ => false,
        }
    }
    pub fn process_mouse(&mut self, mouse_dx: f64, mouse_dy: f64) {
        self.rot_horiz = mouse_dx as f32;
        self.rot_vert = mouse_dy as f32;
    }
    pub fn process_scroll(&mut self, delta: &winit::event::MouseScrollDelta) {
        self.scroll = -match delta {
            winit::event::MouseScrollDelta::LineDelta(_, scroll) => *scroll * 100.0,
            winit::event::MouseScrollDelta::PixelDelta(winit::dpi::PhysicalPosition {
                y: scroll,
                ..
            }) => *scroll as f32,
        };
    }

    pub fn update_camera(&mut self, camera: &mut Camera, dt: std::time::Duration) {
        let dt = dt.as_secs_f32();

        let (yaw_sin, yaw_cos) = camera.yaw.0.sin_cos();
        let forward = Vector3::new(yaw_cos, 0.0, yaw_sin).normalize();
        let right = Vector3::new(-yaw_sin, 0.0, yaw_cos).normalize();
        camera.position += forward * (self.vforward - self.vbackward) * self.speed * dt;
        camera.position += right * (self.vright - self.vleft) * self.speed * dt;

        let (pitch_sin, pitch_cos) = camera.pitch.0.sin_cos();
        let scrollward = Vector3::new(pitch_cos * yaw_cos, pitch_sin, pitch_cos * yaw_sin);
        camera.position += scrollward * self.scroll * self.speed * self.sensitivity * dt;
        self.scroll = 0.0;

        camera.position.y += (self.vup - self.vdown) * self.speed * dt;

        camera.yaw += Rad(self.rot_horiz) * self.sensitivity * dt;
        camera.pitch += Rad(self.rot_vert) * self.sensitivity * dt;

        self.rot_horiz = 0.0;
        self.rot_vert = 0.0;

        let rad_safe_frac = Rad(SAFE_FRAC_PI_2);

        if camera.pitch < -rad_safe_frac {
            camera.pitch = -rad_safe_frac;
        } else if camera.pitch > rad_safe_frac {
            camera.pitch = rad_safe_frac;
        }
    }
}
