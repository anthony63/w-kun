use cgmath::{InnerSpace, Point3, Rad};
pub struct Camera {
    pub position: Point3<f32>,
    pub yaw: Rad<f32>,
    pub pitch: Rad<f32>,
}

impl Camera {
    pub fn new<L: Into<Point3<f32>>, Y: Into<Rad<f32>>, P: Into<Rad<f32>>>(
        position: L,
        yaw: Y,
        pitch: P,
    ) -> Self {
        Self {
            position: position.into(),
            yaw: yaw.into(),
            pitch: pitch.into(),
        }
    }

    pub fn matrix(&self) -> cgmath::Matrix4<f32> {
        let dir =
            cgmath::Vector3::<f32>::new(self.yaw.0.cos(), self.pitch.0.sin(), self.yaw.0.sin());
        cgmath::Matrix4::look_to_rh(
            self.position,
            cgmath::Vector3::<f32>::new(self.yaw.0.cos(), self.pitch.0.sin(), self.yaw.0.sin())
                .normalize(),
            cgmath::Vector3::unit_y(),
        )
    }
}
