use crate::math_util::OPENGL_TO_WGPU_MATRIX;
use cgmath::*;
pub struct Projection {
    aspect: f32,
    fov: Rad<f32>,
    near: f32,
    far: f32,
}

impl Projection {
    pub fn new<F: Into<Rad<f32>>>(width: u32, height: u32, fov: F, near: f32, far: f32) -> Self {
        Self {
            aspect: width as f32 / height as f32,
            fov: fov.into(),
            near,
            far,
        }
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        self.aspect = width as f32 / height as f32;
    }

    pub fn matrix(&self) -> Matrix4<f32> {
        OPENGL_TO_WGPU_MATRIX * perspective(self.fov, self.aspect, self.near, self.far)
    }
}
